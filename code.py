def str_in(str):
    SpecialSym=['@','_','!','#','$','%','^','&','*','<','>','?','/','|','}','{','~',':']
    return_val=True
    return_notfound=False
    if len(str) < 8:
        print('the length of string should be not be greater than 8')
        return_val=False
    if not any(char.isdigit() for char in str):
        print('the string should have at least one numeral')
        return_val=False
    if not any(char.isupper() for char in str):
        print('the string should have at least one uppercase letter')
        return_val=False
    if not any(char.islower() for char in str):
        print('the string should have at least one lowercase letter')
        return_val=False
    if not any(char in SpecialSym for char in str):
        print('the string should have at least one special charactors')
        return_val=False
    if return_val:
        return return_val
    else:
        if("Qwerty" in str):
            print('Donot use any substring')
        return_notfound


str = input('enter the string : ')
print(str_in(str))