MAX = 100


def printDiagonalSums(mat, n):
    main = 0
    for i in range(0, n):
        for j in range(0, n):

            if (i == j):
                main += mat[i][j]

    print("Main Diagonal:", main)

a = [[ 1, 2, 3, 4 ],
     [ 2, 1, 4, 3 ],
     [ 3, 4, 1, 2 ],
      [ 4, 3, 2, 1 ]]

printDiagonalSums(a, 4)